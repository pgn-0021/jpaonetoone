package br.edu.estacio.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.estacio.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();
		
		//Coloque o seu código aqui!!
		Pessoa maria = new Pessoa();
		maria.setNome("Maria");
		maria.setSobrenome("Joana");
		maria.getEndereco().setRua("Rua das Marias");
		
		Pessoa joao = new Pessoa();
		joao.setNome("João");
		joao.setSobrenome("Henrique");
		joao.getEndereco().setRua("Rua João XV");
		
		em.getTransaction().begin();
		
		em.persist(maria);
		em.persist(maria.getEndereco());

		em.persist(joao);
		em.persist(joao.getEndereco());
		
		em.getTransaction().commit();
				
		em.close();
		emf.close();
	}

}
